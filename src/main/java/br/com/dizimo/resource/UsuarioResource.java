package br.com.dizimo.resource;

import br.com.dizimo.model.Usuario;
import br.com.dizimo.service.imp.UsuarioServiceImp;
import br.com.dizimo.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/usuarios")
public class UsuarioResource {

    @Autowired
    private UsuarioServiceImp usuarioService;

    @GetMapping("/usuario/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") Integer id) {
        Optional<Usuario> optionalUsuario = this.usuarioService.findById(id);
        if(optionalUsuario.isPresent()) {
            return ResponseEntity.ok(optionalUsuario.get());
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Usuário não encontrado!");
    }

    @GetMapping("/all")
    public ResponseEntity<?> findAll(Pageable pageable) {
        Iterable<Usuario> usuarios = this.usuarioService.findAll(pageable);
        if (usuarios.iterator().hasNext()) {
            return ResponseEntity.ok().body(usuarios);    
        }
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/save")
    public ResponseEntity<?> save(@RequestBody Usuario usuario) {
        if(!Utils.isNullOrEmpty(usuario.getUsername()) && !Utils.isNullOrEmpty(usuario.getPassword())) {
            return ResponseEntity.status(HttpStatus.CREATED).body(this.usuarioService.create(usuario));
        }
        return ResponseEntity.badRequest().build();
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> update(@PathVariable("id") Integer id, @RequestBody Usuario usuario) {
        if(!Utils.isNullOrEmpty(id) && !Utils.isNullOrEmpty(usuario)) {
            if (!usuario.getId().equals(id)) {
              return ResponseEntity.badRequest().body("Id informado não confere com o id de usuário informado para atualização! Verifique e tente novamente");  
            }
            return ResponseEntity.status(HttpStatus.OK).body(this.usuarioService.update(id, usuario));
        }
        return ResponseEntity.badRequest().build();
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Integer id){
        Optional<Usuario> optionalUsuario = this.usuarioService.findById(id);
        if(optionalUsuario.isPresent()) {
            usuarioService.delete(id);
            return ResponseEntity.ok("Usuário removido com Sucesso!");
        }
        return ResponseEntity.badRequest().build();
    }
}
