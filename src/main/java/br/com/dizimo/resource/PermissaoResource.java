package br.com.dizimo.resource;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Pageable;

import br.com.dizimo.model.Permissao;
import br.com.dizimo.service.imp.PermissaoServiceImp;
import br.com.dizimo.util.Utils;

@CrossOrigin
@RestController
@RequestMapping("/permissoes")
public class PermissaoResource {

    @Autowired
    private PermissaoServiceImp permissaoServiceImp;

    @GetMapping("/permissao/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") Integer id) {
        Optional<Permissao> optionalPermissao = this.permissaoServiceImp.findById(id);
        if(optionalPermissao.isPresent()) {
            return ResponseEntity.ok(optionalPermissao.get());
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Permissão não encontrada!");
    }

    @GetMapping("/all")
    public ResponseEntity<?> findAll(Pageable pageable) {
        Iterable<Permissao> permissoes = this.permissaoServiceImp.findAll(pageable);
        if (permissoes.iterator().hasNext()) {
            return ResponseEntity.ok().body(permissoes);    
        }
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/save")
    public ResponseEntity<?> save(@RequestBody Permissao permissao) {
        if(!Utils.isNullOrEmpty(permissao.getNome()) && !Utils.isNullOrEmpty(permissao.getDescricao())) {
            return ResponseEntity.status(HttpStatus.CREATED).body(this.permissaoServiceImp.create(permissao));
        }
        return ResponseEntity.badRequest().build();
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> update(@PathVariable("id") Integer id, @RequestBody Permissao permissao) {
        if(!Utils.isNullOrEmpty(id) && !Utils.isNullOrEmpty(permissao)) {
            if (!permissao.getId().equals(id)) {
              return ResponseEntity.badRequest().body("Id informado não confere com o id da permissão " 
                + "informada para atualização! Verifique e tente novamente");  
            }
            return ResponseEntity.status(HttpStatus.OK).body(this.permissaoServiceImp.update(id, permissao));
        }
        return ResponseEntity.badRequest().build();
    }
    
    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Integer id){
        Optional<Permissao> optionalPermissao = this.permissaoServiceImp.findById(id);
        if(optionalPermissao.isPresent()) {
            this.permissaoServiceImp.delete(id);
            return ResponseEntity.ok("Permissão removida com Sucesso!");
        }
        return ResponseEntity.badRequest().build();
    }
}