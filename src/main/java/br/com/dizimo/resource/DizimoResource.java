package br.com.dizimo.resource;

import br.com.dizimo.model.Dizimo;
import br.com.dizimo.service.DizimoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/dizimos")
@RestController
@CrossOrigin
public class DizimoResource {

    @Autowired
    private DizimoService dizimoService;

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ResponseEntity<?> getDizimo(){
        return ResponseEntity.ok("ok");
    }

    @GetMapping("/list") //READ
    public ResponseEntity<List<?>> getListAllDizimo(){
        List<Dizimo> listDizimos = this.dizimoService.getAllDizimos();
        return new ResponseEntity<>(listDizimos, listDizimos.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    @GetMapping("/{idDizimo}") //READ
    public ResponseEntity<Dizimo> getDizimiById(@PathVariable("idDizimo") Integer id) {
        Dizimo dizimoResponse = this.dizimoService.findDizimoId(id);
        return ResponseEntity.status(dizimoResponse != null ? HttpStatus.OK : HttpStatus.NOT_FOUND).body(dizimoResponse);
    }

    @PostMapping("/save") //CREATE
    public ResponseEntity<Dizimo> saveDizimo(@RequestBody Dizimo dizimo){
        Dizimo dizimoResponse = dizimoService.salvarDizimo(dizimo);
        return ResponseEntity.status(dizimoResponse != null ? HttpStatus.OK : HttpStatus.BAD_REQUEST).body(dizimoResponse);
    }

    @PutMapping("/update/{idDizimo}") //UPDATE
    public ResponseEntity<String> editarDizimo(@RequestBody Dizimo dizimoAtual, @PathVariable("idDizimo") Integer id){
        Dizimo dizimoResponse = dizimoService.editarDizimo(id, dizimoAtual);
        return ResponseEntity.status(dizimoResponse != null ? HttpStatus.OK : HttpStatus.BAD_REQUEST)
                .body(dizimoResponse!= null ? "Atualizado com Sucesso" : "Ocorreu um erro ao atualizar registro.");
    }

    @DeleteMapping("/delete/{idDizimo}") //DELETE
    public ResponseEntity<?> deletarDizimo(@PathVariable("idDizimo") Integer id) {
        boolean dizimoResponse = dizimoService.deletarDizimo(id);
        return ResponseEntity.status(dizimoResponse ? HttpStatus.OK : HttpStatus.BAD_REQUEST)
                .body(dizimoResponse ? "Registro deletado com sucesso!" : "Ocorreu um erro ao deletar registro.");
    }
}
