package br.com.dizimo.resource;

import br.com.dizimo.model.Pessoa;
import br.com.dizimo.service.PessoaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/pessoas")
@RestController
@CrossOrigin
public class PessoaResource  {

    @Autowired
    private PessoaService pessoaService;

    @GetMapping("/pessoa/{idPessoa}") //READ
    public ResponseEntity<?> getPessoa(@PathVariable("idPessoa") Integer id) {
        Pessoa pessoaResponse = pessoaService.findPessoaId(id);
        return ResponseEntity.status(pessoaResponse != null ? HttpStatus.OK : HttpStatus.BAD_REQUEST).body(pessoaResponse);
    }

    @PostMapping("/save") //CREATE
    public ResponseEntity<Pessoa> salvarPessoa(@RequestBody Pessoa pessoa){
        Pessoa pessoaResponse = pessoaService.salvarPessoa(pessoa);
        return ResponseEntity.status(pessoaResponse != null ? HttpStatus.OK : HttpStatus.BAD_REQUEST).body(pessoaResponse);
    }

    @PutMapping("/update/{idPessoa}") //UPDATE
    public ResponseEntity<?> editarPessoas(@RequestBody Pessoa pessoaAtual, @PathVariable("idPessoa") Integer id) {
        Pessoa pessoaResponse = pessoaService.editarPessoa(pessoaAtual, id);
        return ResponseEntity.status(pessoaResponse != null ? HttpStatus.OK : HttpStatus.BAD_REQUEST)
              .body(pessoaResponse != null ? "Atualizado com Sucesso" : "Ocorreu um erro ao atualizar registro.");
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/delete/{idPessoa}") //DELETE
    public ResponseEntity<?> deletarPessoa(@PathVariable("idPessoa") Integer id){
        Pessoa pessoaResponse = pessoaService.deletarPessoa(id);
        return ResponseEntity.status(pessoaResponse != null ? HttpStatus.OK : HttpStatus.BAD_REQUEST)
               .body(pessoaResponse != null ? "Deletado com Sucesso" : "Ocorreu um erro ao deletar registro.");
    }

    @GetMapping("/list") //READ
    public ResponseEntity<List<?>> getListAllDizimo(){
        return new ResponseEntity<>(this.pessoaService.getAllPesssoas(), HttpStatus.OK);
    }
}
