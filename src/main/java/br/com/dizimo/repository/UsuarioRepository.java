package br.com.dizimo.repository;

import br.com.dizimo.model.Usuario;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepository extends PagingAndSortingRepository<Usuario, Integer> {

    Usuario findByUsername(String username);
    
}
