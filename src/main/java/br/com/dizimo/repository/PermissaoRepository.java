package br.com.dizimo.repository;

import br.com.dizimo.model.Permissao;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PermissaoRepository extends PagingAndSortingRepository<Permissao, Integer> {
}
