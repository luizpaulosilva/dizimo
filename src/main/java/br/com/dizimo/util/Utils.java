package br.com.dizimo.util;

public final class Utils {

    public static boolean isNullOrEmpty(Object field){
        return field == null || field.toString().trim() == "";
    }
}
