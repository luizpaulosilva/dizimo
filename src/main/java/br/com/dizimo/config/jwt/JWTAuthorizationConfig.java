package br.com.dizimo.config.jwt;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import br.com.dizimo.service.imp.UserDetailServiceImp;
import io.jsonwebtoken.Jwts;

public class JWTAuthorizationConfig extends BasicAuthenticationFilter {

    private UserDetailServiceImp userDetailService;
    static final String TOKEN_PREFIX = "Bearer";
    static final long EXPIRATION_TIME = 860_000_000;
    static final String SECRET = "api-dizimo";
    static final String HEADER_STRING = "Authorization";

    public JWTAuthorizationConfig(AuthenticationManager authManager, UserDetailServiceImp userDetailService) {
        super(authManager);
        this.userDetailService = userDetailService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain)
            throws IOException, ServletException {
        String header = req.getHeader(HEADER_STRING);
        if (header == null || !header.startsWith(TOKEN_PREFIX)) {
            chain.doFilter(req, res);
            return;
        }
        SecurityContextHolder.getContext().setAuthentication(getAuthenticationToken(req));
        chain.doFilter(req, res);
    }

    private UsernamePasswordAuthenticationToken getAuthenticationToken(HttpServletRequest req) {
        String token = req.getHeader(HEADER_STRING);
        if (token == null) {
            return null;
        }
        String username = Jwts.parser().setSigningKey(SECRET)
                        .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
                        .getBody()
                        .getSubject();
        UserDetails userDetails = userDetailService.loadUserByUsername(username);
        return username != null ? new UsernamePasswordAuthenticationToken(username, userDetails.getPassword(), 
                userDetails.getAuthorities()) : null;
    }
    
}