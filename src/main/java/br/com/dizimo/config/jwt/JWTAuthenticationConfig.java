package br.com.dizimo.config.jwt;

import java.io.IOException;
import java.util.Date;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import br.com.dizimo.model.Usuario;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JWTAuthenticationConfig extends UsernamePasswordAuthenticationFilter {

    static final String TOKEN_PREFIX = "Bearer";
    static final long EXPIRATION_TIME = 60000;
    static final String SECRET = "api-dizimo";
    static final String HEADER_STRING = "Authorization";
    private AuthenticationManager authenticationManager;

    public JWTAuthenticationConfig(AuthenticationManager authManager) {
        this.authenticationManager = authManager;
	}

	@Override
    public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res)
            throws AuthenticationException {
        Usuario usuario;
        try {
            usuario = new ObjectMapper().readValue(req.getInputStream(), Usuario.class);
            return this.authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(usuario.getUsername(), 
                    usuario.getPassword(), usuario.getPermissoes()));
        } catch (IOException e) {
           throw new RuntimeException(e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest req, HttpServletResponse res, 
            FilterChain chain, Authentication authResult) throws IOException, ServletException {
        String username = ((org.springframework.security.core.userdetails.User) authResult.getPrincipal()).getUsername();
        String TOKEN_JWT = Jwts.builder()
                        .setSubject(username)
                        .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                        .signWith(SignatureAlgorithm.HS512, SECRET)
                        .compact();
        res.addHeader(HEADER_STRING, TOKEN_PREFIX + " " + TOKEN_JWT);
    }
}