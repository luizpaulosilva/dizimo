package br.com.dizimo.service.imp;

import br.com.dizimo.model.Usuario;
import br.com.dizimo.repository.UsuarioRepository;
import br.com.dizimo.service.BaseService;
import br.com.dizimo.util.Utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class UsuarioServiceImp implements BaseService<Usuario> {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Usuario create(Usuario usuario) {
        usuario.setDataCriacao(LocalDateTime.now());
        return this.usuarioRepository.save(setBcryptPassword(usuario));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Usuario update(Integer id, Usuario usuario) {
        Optional<Usuario> optionalUsuario = this.usuarioRepository.findById(id);
        if (optionalUsuario.isPresent()) {
            return this.usuarioRepository.save(setupUpdate(optionalUsuario.get(), usuario));
        }
        throw new RuntimeException("Erro ao atualizar usuário!");
    }

    @Override
    public void delete(Integer id) {
        Optional<Usuario> optionalUsuario = this.usuarioRepository.findById(id);
        optionalUsuario.ifPresent(usuario -> this.usuarioRepository.delete(usuario));
    }

    @Override
    public boolean isPersist(Usuario usuario) {
        return !Utils.isNullOrEmpty(usuario.getId());
    }

    @Override
    public Optional<Usuario> findById(Integer id) {
        return this.usuarioRepository.findById(id);
    }

    @Override
    public Iterable<Usuario> findAll(Pageable pageable) {
        return this.usuarioRepository.findAll(pageable);
    }

    public Usuario setBcryptPassword(Usuario usuario) {
        if (!Utils.isNullOrEmpty(usuario) && !Utils.isNullOrEmpty(usuario.getPassword())) {
            usuario.setPassword(new BCryptPasswordEncoder().encode(usuario.getPassword()));
            return usuario;
        }
        return null;
    }

    private Usuario setupUpdate(Usuario usuarioOld, Usuario usuario) {
        usuario.setDataAtualizacao(LocalDateTime.now());
        usuario.setDataCriacao(usuarioOld.getDataCriacao());
        usuario.setId(Utils.isNullOrEmpty(usuario.getId()) ? usuarioOld.getId() : usuario.getId());
        usuario.setNome(Utils.isNullOrEmpty(usuario.getNome()) ? usuarioOld.getNome() : usuario.getNome());
        usuario.setAtivo(Utils.isNullOrEmpty(usuario.isAtivo()) ? usuarioOld.isAtivo() : usuario.isAtivo()); 
        usuario.setUsername(Utils.isNullOrEmpty(usuario.getUsername()) ? usuarioOld.getUsername() : usuario.getUsername());
        usuario.setPassword(Utils.isNullOrEmpty(usuario.getPassword()) ? usuarioOld.getPassword() : usuario.getPassword());
        usuario.setPermissoes(Utils.isNullOrEmpty(usuario.getPermissoes()) ? usuarioOld.getPermissoes() : usuario.getPermissoes());
        return usuario;
    }
}
