package br.com.dizimo.service.imp;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dizimo.model.Permissao;
import br.com.dizimo.repository.PermissaoRepository;
import br.com.dizimo.service.BaseService;
import br.com.dizimo.util.Utils;

@Service
public class PermissaoServiceImp implements BaseService<Permissao> {

    @Autowired
    private PermissaoRepository permissaoRepository;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Permissao create(Permissao permissao) {
        return this.permissaoRepository.save(permissao);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean isPersist(Permissao permissao) {
        return !Utils.isNullOrEmpty(permissao.getId());
    }

    @Override
    public void delete(Integer id) {
        Optional<Permissao> optionalPermissao = this.permissaoRepository.findById(id);
        optionalPermissao.ifPresent(permissao -> this.permissaoRepository.delete(permissao));
    }

    @Override
    public Permissao update(Integer id, Permissao permissao) {
        Optional<Permissao> optionalPermissao = this.permissaoRepository.findById(id);
        if (optionalPermissao.isPresent()) {
            return this.permissaoRepository.save(setupUpdate(optionalPermissao.get(), permissao));
        }
        throw new RuntimeException("Erro ao atualizar permissão!");
    }

    @Override
    public Optional<Permissao> findById(Integer id) {
        return this.permissaoRepository.findById(id);
    }

    @Override
    public Iterable<Permissao> findAll(Pageable pageable) {
        return this.permissaoRepository.findAll();
    }

    private Permissao setupUpdate(Permissao permissaoOld, Permissao permissao) {
        permissao.setId(Utils.isNullOrEmpty(permissao.getId()) ? permissaoOld.getId() : permissao.getId());
        permissao.setNome(Utils.isNullOrEmpty(permissao.getNome()) ? permissaoOld.getNome() : permissao.getNome());
        permissao.setDescricao(Utils.isNullOrEmpty(permissao.getDescricao()) ? permissaoOld.getDescricao() : permissao.getDescricao());
        return permissao;
    }
}