package br.com.dizimo.service.imp;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import br.com.dizimo.model.Usuario;
import br.com.dizimo.repository.UsuarioRepository;

@Service
public class UserDetailServiceImp implements UserDetailsService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Usuario usuario = Optional.ofNullable(this.usuarioRepository.findByUsername(username))
                .orElseThrow(() -> new UsernameNotFoundException("Usuário " + username + " não encontrado!"));
        return new User(usuario.getUsername(), usuario.getPassword(), usuario.getPermissoes());
    }
}