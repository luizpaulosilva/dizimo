package br.com.dizimo.service;

import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface BaseService<T> {

    T create(T t);

    boolean isPersist(T t);

    void delete (Integer id);

    T update(Integer id, T t);

    Optional<T> findById(Integer id);

    Iterable<T> findAll(Pageable pageable);
}
