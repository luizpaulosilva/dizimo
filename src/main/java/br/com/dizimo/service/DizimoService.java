package br.com.dizimo.service;

import br.com.dizimo.model.Dizimo;
import br.com.dizimo.repository.DizimoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Service
public class DizimoService {

    @Autowired
    private PessoaService pessoaService;

    @Autowired
    private DizimoRepository dizimoRepository;

    @Transactional
    public Dizimo salvarDizimo(Dizimo dizimo){
        if(dizimo.getId() == null && dizimo.getPessoa().getId() != null){
            dizimo.setPessoa(this.pessoaService.findPessoaId(dizimo.getPessoa().getId()));
            dizimo.setDataPagamento(new Date());
            return this.dizimoRepository.save(dizimo);
        }
        return null;
    }

    @Transactional
    public Dizimo editarDizimo(Integer id, Dizimo dizimoAtual){
        if(id != null) {
            Dizimo oldDizimo = dizimoRepository.findDizimoById(id);
            if (oldDizimo != null) {
                oldDizimo.setDataPagamento(new Date());
                oldDizimo.setMes(dizimoAtual.getMes());
                oldDizimo.setValor(dizimoAtual.getValor());
                oldDizimo.setObservacao(dizimoAtual.getObservacao());
                return this.dizimoRepository.save(oldDizimo);
            }
        }
        return null;
    }

    public boolean deletarDizimo(Integer id){
        if(id != null) {
            Dizimo dizimo = dizimoRepository.findDizimoById(id);
            if (dizimo.getId() != null) {
                this.dizimoRepository.delete(dizimo);
                return true;
            }
        }
        return false;
    }

    public Dizimo findDizimoId(Integer id){
       return this.dizimoRepository.findDizimoById(id);
    }

    public List<Dizimo> getAllDizimos() {
        return this.dizimoRepository.findAll();
    }
}
