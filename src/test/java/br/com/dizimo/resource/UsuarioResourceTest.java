package br.com.dizimo.resource;

import static org.mockito.ArgumentMatchers.isA;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import br.com.dizimo.model.Permissao;
import br.com.dizimo.model.Usuario;
import br.com.dizimo.service.imp.UserDetailServiceImp;
import br.com.dizimo.service.imp.UsuarioServiceImp;

@RunWith(SpringRunner.class)
@WebMvcTest(UsuarioResource.class)
public class UsuarioResourceTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UsuarioServiceImp usuarioServiceImp;

    @MockBean
    private UserDetailServiceImp userDetailServiceImp;

    private Usuario usuario;
    private List<Usuario> usuarios;
    private ObjectMapper objectMapper;
    private final String ROLE_ADMIN = "ADMIN";
    private final String USER = "teste@teste.com";
    private final String PASSWORD = "$2a$10$NyXwJZXnAi8.uK85tvDl6.YhXdfSEjs1BvCFYGg28V8znU9czqJ42";

    @Before
    public void init() {
        usuario = buildUsuario(1,"Luiz", "luiz@teste.com", "123456");
        objectMapper = new ObjectMapper();
        usuarios = new ArrayList<>();
        usuarios.addAll(buildUsuarios());
    }

    @Test
    @WithMockUser
    public void findUsuarioById_GET_OK() throws Exception {
        Mockito.when(this.usuarioServiceImp.findById(usuario.getId())).thenReturn(Optional.of(usuario));
        this.mockMvc.perform(MockMvcRequestBuilders.get("/usuarios/usuario/" + usuario.getId())
                .characterEncoding("UTF-8")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$['id']", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$['nome']", Matchers.equalToIgnoringWhiteSpace(usuario.getNome())))
                .andExpect(MockMvcResultMatchers.jsonPath("$['username']", Matchers.is(usuario.getUsername())));
    }

    @Test
    @WithMockUser
    public void findUsuarioById_GET_NOT_FOUND() throws Exception {
        Mockito.when(this.usuarioServiceImp.findById(4)).thenReturn(Optional.empty());
        this.mockMvc.perform(MockMvcRequestBuilders.get("/usuarios/usuario/4")
                .characterEncoding("UTF-8")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.is("Usuário não encontrado!")));
    }

    @Test
    @WithMockUser
    public void findAllUsuario_GET_OK() throws Exception {
        Mockito.when(this.usuarioServiceImp.findAll(isA(Pageable.class))).thenReturn(new PageImpl<>(usuarios));
        this.mockMvc.perform(MockMvcRequestBuilders.get("/usuarios/all")
                .characterEncoding("UTF-8")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$['content'][0].['id']", Matchers.is(usuarios.get(0).getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$['content'][0].['nome']", Matchers.is(usuarios.get(0).getNome())))
                .andExpect(MockMvcResultMatchers.jsonPath("$['totalElements']",  Matchers.is(usuarios.size())));
    }

    @Test
    @WithMockUser
    public void findAllUsuario_GET_NO_CONTENT() throws Exception {
        Mockito.when(this.usuarioServiceImp.findAll(isA(Pageable.class))).thenReturn(new PageImpl<>(new ArrayList<>()));
        this.mockMvc.perform(MockMvcRequestBuilders.get("/usuarios/all")
                .characterEncoding("UTF-8")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    @WithMockUser
    public void saveUsuario_POST_OK() throws Exception {
        Mockito.when(this.usuarioServiceImp.create(usuario)).thenReturn(usuario);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/usuarios/save")
                .content(objectMapper.writeValueAsString(usuario))
                .characterEncoding("UTF-8")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$['id']", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$['nome']", Matchers.equalToIgnoringWhiteSpace(usuario.getNome())))
                .andExpect(MockMvcResultMatchers.jsonPath("$['username']", Matchers.is(usuario.getUsername())));
    }

    @Test
    @WithMockUser
    public void updateUsuario_PUT_OK() throws Exception {
        Usuario usuarioAtualizado = buildUpdateUsuario(usuario);
        Mockito.when(this.usuarioServiceImp.update(usuario.getId(), usuarioAtualizado)).thenReturn(usuarioAtualizado);
        this.mockMvc.perform(MockMvcRequestBuilders.put("/usuarios/update/" + usuario.getId())
                .content(objectMapper.writeValueAsString(usuarioAtualizado))
                .characterEncoding("UTF-8")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$['id']", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$['nome']", Matchers.equalToIgnoringWhiteSpace(usuarioAtualizado.getNome())))
                .andExpect(MockMvcResultMatchers.jsonPath("$['username']", Matchers.is(usuarioAtualizado.getUsername())));
    }

    @Test
    @WithMockUser
    public void updateUsuario_POST_METHOD_NOT_ALLOWED() throws Exception {
        Usuario usuarioAtualizado = buildUpdateUsuario(usuario);
        Mockito.when(this.usuarioServiceImp.update(usuario.getId(), usuarioAtualizado)).thenReturn(usuarioAtualizado);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/usuarios/update/" + usuario.getId())
                .content(objectMapper.writeValueAsString(usuarioAtualizado))
                .characterEncoding("UTF-8")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isMethodNotAllowed());
    }

    @Test
    @WithMockUser(username = USER, password = PASSWORD, roles = { ROLE_ADMIN })
    public void deleteUsuario_DELETE_OK() throws Exception {
        Mockito.when(this.usuarioServiceImp.findById(usuario.getId())).thenReturn(Optional.of(usuario));
        Mockito.doNothing().when(this.usuarioServiceImp).delete(usuario.getId());
        this.mockMvc.perform(MockMvcRequestBuilders.delete("/usuarios/delete/" + usuario.getId())
                .characterEncoding("UTF-8")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.is("Usuário removido com Sucesso!")));
    }

    @Test
    @WithMockUser
    public void deleteUsuario_DELETE_FORBIDDEN() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.delete("/usuarios/delete/" + usuario.getId())
                .characterEncoding("UTF-8")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    private Usuario buildUsuario(Integer id, String nome, String username, String password) {
        return Usuario.builder().id(id).nome(nome).username(username).password(password).permissoes(buildPermissoes())
                .ativo(Boolean.TRUE).build();
    }

    private List<Usuario> buildUsuarios() {
        return Arrays.asList(buildUsuario(2,"josé", "jose@teste.com", "jose#@$123"),
            buildUsuario(3, "maria", "maria@teste.com", "%¨&123456"));
    }

    private List<Permissao> buildPermissoes() {
        return Arrays.asList(Permissao.builder().nome("ROLE_ADMIN").descricao("ROLE_ADMIN").build(),
            Permissao.builder().nome("ROLE_USER").descricao("ROLE_USER").build());
    }

    private Usuario buildUpdateUsuario(Usuario usuarioAtualizado) {
        usuarioAtualizado.setNome(usuarioAtualizado.getNome() + " Atualizado");
        usuarioAtualizado.setUsername("atualizado@teste.com");
        return usuarioAtualizado;
    }

}
