package br.com.dizimo.resource;

import static org.mockito.ArgumentMatchers.isA;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import br.com.dizimo.model.Permissao;
import br.com.dizimo.service.imp.PermissaoServiceImp;
import br.com.dizimo.service.imp.UserDetailServiceImp;

@RunWith(SpringRunner.class)
@WebMvcTest(PermissaoResource.class)
public class PermissaoResourceTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PermissaoServiceImp permissaoServiceImp;

    @MockBean
    private UserDetailServiceImp userDetailServiceImp;

    private Permissao permissao;
    private ObjectMapper objectMapper;
    private List<Permissao> permissoes;
    private final String ROLE_ADMIN = "ADMIN";
    private final String USER = "teste@teste.com";
    private final String PASSWORD = "$2a$10$NyXwJZXnAi8.uK85tvDl6.YhXdfSEjs1BvCFYGg28V8znU9czqJ42";

    @Before
    public void init() {
        permissao = buildPermissao(1, "ROLE_ADMIN", "ROLE_ADMIN");
        objectMapper = new ObjectMapper();
        permissoes = new ArrayList<>();
        permissoes.addAll(buildPermissoes());
    }

    @Test
    @WithMockUser
    public void findPermissaoById_GET_OK() throws Exception {
        Mockito.when(this.permissaoServiceImp.findById(permissao.getId())).thenReturn(Optional.of(permissao));
        this.mockMvc.perform(MockMvcRequestBuilders.get("/permissoes/permissao/" + permissao.getId())
                .characterEncoding("UTF-8")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$['id']", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$['nome']", Matchers.equalToIgnoringWhiteSpace(permissao.getNome())))
                .andExpect(MockMvcResultMatchers.jsonPath("$['descricao']", Matchers.is(permissao.getDescricao())));
    }

    @Test
    @WithMockUser
    public void findPermissaoById_GET_NOT_FOUND() throws Exception {
        Mockito.when(this.permissaoServiceImp.findById(4)).thenReturn(Optional.empty());
        this.mockMvc.perform(MockMvcRequestBuilders.get("/permissoes/permissao/4")
                .characterEncoding("UTF-8")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.is("Permissão não encontrada!")));
    }

    @Test
    @WithMockUser
    public void findAllPermissao_GET_OK() throws Exception {
        Mockito.when(this.permissaoServiceImp.findAll(isA(Pageable.class))).thenReturn(new PageImpl<>(permissoes));
        this.mockMvc.perform(MockMvcRequestBuilders.get("/permissoes/all")
                .characterEncoding("UTF-8")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$['content'][0].['id']", Matchers.is(permissoes.get(0).getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$['content'][0].['nome']", Matchers.is(permissoes.get(0).getNome())))
                .andExpect(MockMvcResultMatchers.jsonPath("$['content'][0].['descricao']", Matchers.is(permissoes.get(0).getDescricao())))
                .andExpect(MockMvcResultMatchers.jsonPath("$['totalElements']",  Matchers.is(permissoes.size())));
    }

    @Test
    @WithMockUser
    public void findAllPermissao_GET_NO_CONTENT() throws Exception {
        Mockito.when(this.permissaoServiceImp.findAll(isA(Pageable.class))).thenReturn(new PageImpl<>(new ArrayList<>()));
        this.mockMvc.perform(MockMvcRequestBuilders.get("/permissoes/all")
                .characterEncoding("UTF-8")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    @WithMockUser
    public void savePermissao_POST_OK() throws Exception {
        Mockito.when(this.permissaoServiceImp.create(permissao)).thenReturn(permissao);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/permissoes/save")
                .content(objectMapper.writeValueAsString(permissao))
                .characterEncoding("UTF-8")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$['id']", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$['nome']", Matchers.equalToIgnoringWhiteSpace(permissao.getNome())))
                .andExpect(MockMvcResultMatchers.jsonPath("$['descricao']", Matchers.is(permissao.getDescricao())));
    }

    @Test
    @WithMockUser
    public void updatePermissao_PUT_OK() throws Exception {
        Permissao permissaoAtualizada = buildUpdatePermissao(permissao);
        Mockito.when(this.permissaoServiceImp.update(permissao.getId(), permissaoAtualizada)).thenReturn(permissaoAtualizada);
        this.mockMvc.perform(MockMvcRequestBuilders.put("/permissoes/update/" + permissao.getId())
                .content(objectMapper.writeValueAsString(permissaoAtualizada))
                .characterEncoding("UTF-8")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$['id']", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$['nome']", Matchers.equalToIgnoringWhiteSpace(permissaoAtualizada.getNome())))
                .andExpect(MockMvcResultMatchers.jsonPath("$['descricao']", Matchers.is(permissaoAtualizada.getDescricao())));
    }

    @Test
    @WithMockUser
    public void updatePermissao_POST_METHOD_NOT_ALLOWED() throws Exception {
        Permissao permissaoAtualizada = buildUpdatePermissao(permissao);
        Mockito.when(this.permissaoServiceImp.update(permissao.getId(), permissaoAtualizada)).thenReturn(permissaoAtualizada);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/permissoes/update/" + permissao.getId())
                .content(objectMapper.writeValueAsString(permissaoAtualizada))
                .characterEncoding("UTF-8")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isMethodNotAllowed());
    }

    @Test
    @WithMockUser(username = USER, password = PASSWORD, roles = { ROLE_ADMIN })
    public void deletePermissao_DELETE_OK() throws Exception {
        Mockito.when(this.permissaoServiceImp.findById(permissao.getId())).thenReturn(Optional.of(permissao));
        Mockito.doNothing().when(this.permissaoServiceImp).delete(permissao.getId());
        this.mockMvc.perform(MockMvcRequestBuilders.delete("/permissoes/delete/" + permissao.getId())
                .characterEncoding("UTF-8")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.is("Permissão removida com Sucesso!")));
    }

    @Test
    @WithMockUser
    public void deletePermissao_DELETE_FORBIDDEN() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.delete("/permissoes/delete/" + permissao.getId())
                .characterEncoding("UTF-8")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    private Permissao buildUpdatePermissao(Permissao permissaoAtualizada) {
        permissaoAtualizada.setNome(permissaoAtualizada.getNome() + " Atualizado");
        permissaoAtualizada.setDescricao(permissaoAtualizada.getDescricao() + " Atualizado");
        return permissaoAtualizada;
    }

    private Permissao buildPermissao(Integer id, String nome, String descricao) {
        return Permissao.builder().id(id).nome(nome).descricao(descricao).build();
    }

    private List<Permissao> buildPermissoes() {
        return Arrays.asList(permissao, buildPermissao(2, "ROLE_USER", "ROLE_USER"));
    }
}