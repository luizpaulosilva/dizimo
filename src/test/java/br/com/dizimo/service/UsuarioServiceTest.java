package br.com.dizimo.service;

import br.com.dizimo.model.Permissao;
import br.com.dizimo.model.Usuario;
import br.com.dizimo.repository.UsuarioRepository;
import br.com.dizimo.service.imp.UsuarioServiceImp;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.exceptions.base.MockitoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
public class UsuarioServiceTest {

    @Autowired
    private UsuarioServiceImp usuarioService;

    @MockBean
    private UsuarioRepository usuarioRepository;

    private Usuario usuario;
    private List<Permissao> permissoes;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @TestConfiguration
    static class UsuarioServiceTestContextConfiguration {

        @Bean
        public UsuarioServiceImp usuarioService() {
            return new UsuarioServiceImp();
        }
    }

    @Before
    public void init() {
        permissoes = new ArrayList<>();
        permissoes.addAll(buildPermissoes());
        bCryptPasswordEncoder = new BCryptPasswordEncoder();
        usuario = Usuario.builder()
                .nome("Luiz Silva")
                .username("luiz@teste.com")
                .password(bCryptPasswordEncoder.encode("123456"))
                .ativo(Boolean.TRUE)
                .permissoes(permissoes)
                .build();
        usuario.setId(1);
    }

    @Test
    public void shouldRegisterUser() {
        Mockito.when(this.usuarioRepository.save(usuario)).thenReturn(usuario);
        Usuario usuarioAdicionado = this.usuarioService.create(usuario);
        Mockito.verify(this.usuarioRepository).save(usuario);
        Mockito.verify(this.usuarioRepository, Mockito.times(1)).save(usuario);
        Assert.assertEquals(usuario, usuarioAdicionado);
        Assert.assertNotNull(usuarioAdicionado.getId());
        Assert.assertEquals(usuario.getId(), usuarioAdicionado.getId());
    }

    @Test(expected = MockitoException.class)
    public void shouldOccurExceptionWhenGivenNullRegisterUser() {
        Usuario usuarioNomeNull = usuario;
        usuarioNomeNull.setNome(null);
        Mockito.when(this.usuarioRepository.save(usuarioNomeNull)).thenThrow(MockitoException.class);
        this.usuarioService.create(usuarioNomeNull);
        Mockito.verify(this.usuarioRepository, Mockito.times(1)).save(usuarioNomeNull);
    }

    @Test
    public void shouldUpdateUser() {
        Mockito.when(this.usuarioRepository.save(usuario)).thenReturn(usuario);
        Usuario usuarioAdicionado = this.usuarioService.create(usuario);
        Mockito.verify(this.usuarioRepository).save(usuario);
        Mockito.verify(this.usuarioRepository, Mockito.times(1)).save(usuario);
        Assert.assertNotNull(usuarioAdicionado.getId());
        Assert.assertEquals(usuario.getId(), usuarioAdicionado.getId());
        Assert.assertFalse(bCryptPasswordEncoder.matches("654321", usuarioAdicionado.getPassword()));

        Usuario usuarioAtualizado = this.updateUser();
        Mockito.when(this.usuarioRepository.findById(usuario.getId())).thenReturn(Optional.of(usuario));
        Mockito.when(this.usuarioRepository.save(usuarioAtualizado)).thenReturn(usuarioAtualizado);
        this.usuarioService.update(usuarioAdicionado.getId(), usuarioAtualizado);
        Mockito.verify(this.usuarioRepository, Mockito.times(2)).save(usuarioAtualizado);
        Assert.assertEquals(usuario.getId(), usuarioAtualizado.getId());
        Assert.assertTrue(bCryptPasswordEncoder.matches("Password Atualizado", usuarioAtualizado.getPassword()));
    }

    @Test
    public void shouldDeleteUser() {
        Mockito.when(this.usuarioRepository.findById(usuario.getId())).thenReturn(Optional.of(usuario));
        Mockito.doNothing().when(this.usuarioRepository).delete(usuario);
        this.usuarioService.delete(usuario.getId());
        Mockito.verify(this.usuarioRepository, Mockito.times(1)).delete(usuario);
    }

    @Test(expected = Exception.class)
    public void shouldOccurExceptionWhenGivenNullDeleteUser() {
        Mockito.doThrow().when(this.usuarioRepository).delete(Mockito.isNull());
        this.usuarioService.delete(null);
        Mockito.verify(this.usuarioRepository, Mockito.times(1)).delete(null);
    }

    @Test(expected = Exception.class)
    public void shouldOccurExceptionWhenGivenNullUpdateUser() {
        Usuario usuarioAtualizado = this.updateUser();
        usuarioAtualizado.setNome(null);
        Mockito.doThrow().when(this.usuarioRepository).save(Mockito.isNull());
        this.usuarioService.update(usuarioAtualizado.getId(), usuarioAtualizado);
        Mockito.verify(this.usuarioRepository, Mockito.times(1)).save(usuarioAtualizado);
    }

    private List<Permissao> buildPermissoes() {
        return Arrays.asList(Permissao.builder().nome("ROLE_ADMIN").descricao("ROLE_ADMIN").build(),
            Permissao.builder().nome("ROLE_USER").descricao("ROLE_USER").build());
    }

    private Usuario updateUser() {
        this.usuario.setNome("Nome Atualizado");
        this.usuario.setPassword(bCryptPasswordEncoder.encode("Password Atualizado"));
        return usuario;
    }

}
