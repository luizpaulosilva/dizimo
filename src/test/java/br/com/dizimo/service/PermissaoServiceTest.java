package br.com.dizimo.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.exceptions.base.MockitoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.dizimo.model.Permissao;
import br.com.dizimo.repository.PermissaoRepository;
import br.com.dizimo.service.imp.PermissaoServiceImp;

@RunWith(SpringRunner.class)
public class PermissaoServiceTest {

    @Autowired
    private PermissaoServiceImp permissaoServiceImp;

    @MockBean
    private PermissaoRepository permissaoRepository;

    private Permissao permissao;
    private List<Permissao> permissoes;

    @TestConfiguration
    static class PermissaoServiceTestContextConfiguration {

        @Bean
        public PermissaoServiceImp permissaoServiceImp() {
            return new PermissaoServiceImp();
        }
    }

    @Before
    public void init() {
        permissoes = new ArrayList<>();
        permissao = buildPermissao(1, "ROLE_USER", "ROLE_USER");
        permissoes.addAll(buildPermissoes());
    }

    @Test
    public void shouldRegisterPermissao() {
        Mockito.when(this.permissaoRepository.save(permissao)).thenReturn(permissao);
        Permissao permissaoAdicionada = this.permissaoServiceImp.create(permissao);
        Mockito.verify(this.permissaoRepository).save(permissao);
        Mockito.verify(this.permissaoRepository, Mockito.times(1)).save(permissao);
        Assert.assertEquals(permissao, permissaoAdicionada);
        Assert.assertNotNull(permissaoAdicionada.getId());
        Assert.assertEquals(permissao.getId(), permissaoAdicionada.getId());
    }

    @Test(expected = MockitoException.class)
    public void shouldOccurExceptionWhenGivenNullRegisterPermissao() {
        Permissao permissaoNomeNull = permissao;
        permissaoNomeNull.setNome(null);
        Mockito.when(this.permissaoRepository.save(permissaoNomeNull)).thenThrow(MockitoException.class);
        this.permissaoServiceImp.create(permissaoNomeNull);
        Mockito.verify(this.permissaoRepository, Mockito.times(1)).save(permissaoNomeNull);
    }

    @Test
    public void shouldUpdatePermissao() {
        Mockito.when(this.permissaoRepository.save(permissao)).thenReturn(permissao);
        Permissao permissaoAdicionada = this.permissaoServiceImp.create(permissao);
        Mockito.verify(this.permissaoRepository).save(permissao);
        Mockito.verify(this.permissaoRepository, Mockito.times(1)).save(permissao);
        Assert.assertNotNull(permissaoAdicionada.getId());
        Assert.assertEquals(permissao.getId(), permissaoAdicionada.getId());

        Permissao permissaoAtualizada = this.updatePermissao(permissaoAdicionada);
        Mockito.when(this.permissaoRepository.findById(permissaoAtualizada.getId())).thenReturn(Optional.of(permissaoAtualizada));
        Mockito.when(this.permissaoRepository.save(permissaoAtualizada)).thenReturn(permissaoAtualizada);
        this.permissaoServiceImp.update(permissaoAdicionada.getId(), permissaoAtualizada);
        Mockito.verify(this.permissaoRepository, Mockito.times(2)).save(permissaoAtualizada);
        Assert.assertEquals(permissaoAdicionada.getId(), permissaoAtualizada.getId());
    }

    @Test
    public void shouldDeletePermissao() {
        Mockito.when(this.permissaoRepository.findById(permissao.getId())).thenReturn(Optional.of(permissao));
        Mockito.doNothing().when(this.permissaoRepository).delete(permissao);
        this.permissaoServiceImp.delete(permissao.getId());
        Mockito.verify(this.permissaoRepository, Mockito.times(1)).delete(permissao);
    }

    @Test(expected = Exception.class)
    public void shouldOccurExceptionWhenGivenNullDeletePermissao() {
        Mockito.doThrow().when(this.permissaoRepository).delete(Mockito.isNull());
        this.permissaoServiceImp.delete(null);
        Mockito.verify(this.permissaoRepository, Mockito.times(1)).delete(null);
    }

    @Test(expected = Exception.class)
    public void shouldOccurExceptionWhenGivenNullUpdateUser() {
        Permissao permissaoAtualizada = this.updatePermissao(permissao);
        permissaoAtualizada.setNome(null);
        Mockito.doThrow().when(this.permissaoRepository).save(Mockito.isNull());
        this.permissaoServiceImp.update(permissaoAtualizada.getId(), permissaoAtualizada);
        Mockito.verify(this.permissaoRepository, Mockito.times(1)).save(permissaoAtualizada);
    }

    private Permissao buildPermissao(Integer id, String nome, String descricao) {
        return Permissao.builder().id(id).nome(nome).descricao(descricao).build();
    }

    private List<Permissao> buildPermissoes() {
        return Arrays.asList(permissao, buildPermissao(2, "ROLE_ADMIN", "ROLE_ADMIN"));
    }

    private Permissao updatePermissao(Permissao permissaoAtualizar) {
        permissaoAtualizar.setNome(permissaoAtualizar.getNome() + " Atualizado");
        permissaoAtualizar.setDescricao(permissaoAtualizar.getDescricao() + " Atualizado");
        return permissaoAtualizar;
    }
}