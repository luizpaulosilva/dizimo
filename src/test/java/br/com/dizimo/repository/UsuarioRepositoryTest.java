package br.com.dizimo.repository;

import br.com.dizimo.model.Permissao;
import br.com.dizimo.model.Usuario;
import org.assertj.core.api.Assertions;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UsuarioRepositoryTest {

    @Autowired
    private PermissaoRepository permissaoRepository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    private Usuario usuario;
    private List<Permissao> permissoes;
    private List<Usuario> usuarios;
    private List<Usuario> usuariosFindAll;
    private static String PASSWORD = "#123456@";
    private static String PASSWORD_UPDATE = "#654321@";
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Before
    public void init() {
        permissoes = new ArrayList<>();
        usuarios = new ArrayList<>();
        usuariosFindAll = new ArrayList<>();
        bCryptPasswordEncoder = new BCryptPasswordEncoder();
        usuario = buildUser("Luiz", "luiz@teste.com", PASSWORD);
    }

    @Test
    public void findAllUsers() {
        permissoes.addAll(buildPermissoes());
        this.permissaoRepository.saveAll(permissoes);
        Assertions.assertThat(permissoes).isNotEmpty();
        Assertions.assertThat(permissoes.get(0).getId()).isNotNull();
        usuarios.addAll(buildUsuarios());
        this.usuarioRepository.saveAll(usuarios);
        usuariosFindAll.addAll(Lists.newArrayList(this.usuarioRepository.findAll()));
        Assertions.assertThat(usuariosFindAll).isNotNull().isNotEmpty();
        Assertions.assertThat(usuariosFindAll.size()).isEqualTo(usuarios.size());
        Assertions.assertThat(usuariosFindAll.get(0).getId()).isEqualTo(usuarios.get(0).getId());
    }

    @Test
    public void findUserById() {
        permissoes.addAll(buildPermissoes());
        this.permissaoRepository.saveAll(permissoes);
        this.usuarioRepository.save(usuario);
        Optional<Usuario> optionalUsuario = this.usuarioRepository.findById(usuario.getId());
        Assertions.assertThat(optionalUsuario).isNotNull().isNotEmpty();
        Assertions.assertThat(optionalUsuario.get().isAtivo()).isTrue();
        Assertions.assertThat(optionalUsuario.get().getId()).isNotNull();
        Assertions.assertThat(optionalUsuario.get().getId()).isEqualTo(usuario.getId());
        Assertions.assertThat(optionalUsuario.get().getNome()).isEqualToIgnoringCase("LUIZ");
        Assertions.assertThat(bCryptPasswordEncoder.matches(PASSWORD, optionalUsuario.get().getPassword())).isTrue();
    }

    @Test
    public void createUser() {
        permissoes.addAll(buildPermissoes());
        this.permissaoRepository.saveAll(permissoes);
        Assertions.assertThat(permissoes).isNotEmpty();
        Assertions.assertThat(permissoes.get(0).getId()).isNotNull();
        this.usuarioRepository.save(usuario);
        Assertions.assertThat(usuario).isNotNull();
        Assertions.assertThat(usuario.getId()).isNotNull();
        Assertions.assertThat(bCryptPasswordEncoder.matches(PASSWORD, usuario.getPassword())).isTrue();
    }

    @Test
    public void updateUser() {
        permissoes.addAll(buildPermissoes());
        this.permissaoRepository.saveAll(permissoes);
        Assertions.assertThat(permissoes).isNotEmpty();
        Assertions.assertThat(permissoes.get(0).getId()).isNotNull();
        this.usuarioRepository.save(usuario);
        buildUpdateUser();
        this.usuarioRepository.save(usuario);
        Assertions.assertThat(usuario).isNotNull();
        Assertions.assertThat(usuario.isAtivo()).isFalse();
        Assertions.assertThat(usuario.getUsername()).isEqualTo("teste@teste.com");
        Assertions.assertThat(bCryptPasswordEncoder.matches(PASSWORD_UPDATE, usuario.getPassword())).isTrue();
    }

    @Test
    public void deleteUser() {
        permissoes.addAll(buildPermissoes());
        this.permissaoRepository.saveAll(permissoes);
        Assertions.assertThat(permissoes).isNotEmpty();
        Assertions.assertThat(permissoes.get(0).getId()).isNotNull();
        this.usuarioRepository.save(usuario);
        this.usuarioRepository.delete(usuario);
        Assertions.assertThat(this.usuarioRepository.findById(usuario.getId())).isEmpty();
    }

    private Usuario buildUser(String name, String username, String password) {
        return Usuario.builder().nome(name).username(username)
                .password(bCryptPasswordEncoder.encode(password))
                .ativo(Boolean.TRUE).permissoes(permissoes).build();
    }

    private List<Permissao> buildPermissoes() {
        return Arrays.asList(Permissao.builder().nome("ROLE_ADMIN").descricao("ROLE_ADMIN").build(),
                Permissao.builder().nome("ROLE_USER").descricao("ROLE_USER").build());
    }

    private List<Usuario> buildUsuarios() {
        return Arrays.asList(buildUser("José Teste", "jose@teste.com", "jose123"),
                buildUser("Maria Teste", "maria@teste.com", "maria456"));
    }

    private void buildUpdateUser() {
        usuario.setAtivo(Boolean.FALSE);
        usuario.setUsername("teste@teste.com");
        usuario.setPassword(bCryptPasswordEncoder.encode(PASSWORD_UPDATE));
    }
}
