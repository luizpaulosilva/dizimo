package br.com.dizimo.repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.dizimo.model.Permissao;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PermissaoRepositoryTest {

    @Autowired
    private PermissaoRepository permissaoRepository;

    private Permissao permissao;
    private List<Permissao> permissoes;
    private List<Permissao> permissoesfindAll;

    @Before
    public void init() {
        permissoes = new ArrayList<>();
        permissoesfindAll = new ArrayList<>();
        permissao = buildPermissao("ROLE_ADMIN", "ROLE_ADMIN");
    }

    @Test
    public void findAllPermissoes() {
        permissoes.addAll(buildPermissoes());
        this.permissaoRepository.saveAll(permissoes);
        Assertions.assertThat(permissoes).isNotEmpty();
        Assertions.assertThat(permissoes.get(0).getId()).isNotNull();

        permissoesfindAll.addAll(Lists.newArrayList(this.permissaoRepository.findAll()));
        Assertions.assertThat(permissoesfindAll).isNotNull().isNotEmpty();
        Assertions.assertThat(permissoesfindAll.size()).isEqualTo(permissoes.size());
        Assertions.assertThat(permissoesfindAll.get(0).getId()).isEqualTo(permissoes.get(0).getId());
    }

    @Test
    public void findPermissaoById() {
        this.permissaoRepository.save(permissao);
        Optional<Permissao> optionalPermissao = this.permissaoRepository.findById(permissao.getId());
        Assertions.assertThat(optionalPermissao).isNotNull().isNotEmpty();
        Assertions.assertThat(optionalPermissao.get().getId()).isEqualTo(permissao.getId());
        Assertions.assertThat(optionalPermissao.get().getNome()).isNotNull();
        Assertions.assertThat(optionalPermissao.get().getDescricao()).isNotNull();
        Assertions.assertThat(optionalPermissao.get().getNome()).isEqualTo(permissao.getNome());
        Assertions.assertThat(optionalPermissao.get().getDescricao()).isEqualTo(permissao.getDescricao());
    }

    @Test
    public void createPermissao() {
        this.permissaoRepository.save(permissao);
        Assertions.assertThat(permissao).isNotNull();
        Assertions.assertThat(permissao.getId()).isNotNull();
        Assertions.assertThat(this.permissaoRepository.findById(permissao.getId())).isNotEmpty();
    }

    @Test
    public void updatePermissao() {
        this.permissaoRepository.save(permissao);
        Assertions.assertThat(permissao).isNotNull();
        Assertions.assertThat(permissao.getId()).isNotNull();
        Assertions.assertThat(permissao.getNome()).isEqualToIgnoringCase("ROLE_ADMIN");
        Assertions.assertThat(permissao.getDescricao()).isEqualToIgnoringCase("ROLE_ADMIN");
        buildUpdatePermissao();
        this.permissaoRepository.save(permissao);
        Assertions.assertThat(permissao).isNotNull();
        Assertions.assertThat(permissao.getId()).isNotNull();
        Assertions.assertThat(permissao.getNome()).isEqualToIgnoringCase("ROLE_ADMIN Atualizado");
        Assertions.assertThat(permissao.getDescricao()).isEqualToIgnoringCase("ROLE_ADMIN Atualizado");
    }

    @Test
    public void deletePermissao() {
        this.permissaoRepository.save(permissao);
        Assertions.assertThat(permissao).isNotNull();
        Assertions.assertThat(permissao.getId()).isNotNull();
        this.permissaoRepository.delete(permissao);
        Assertions.assertThat(this.permissaoRepository.findById(permissao.getId())).isEmpty();
    }

    private Permissao buildPermissao(String nome, String descricao) {
        return Permissao.builder().nome(nome).descricao(descricao).build();
    }

    private void buildUpdatePermissao() {
        this.permissao.setNome(permissao.getNome() + " Atualizado");
        this.permissao.setDescricao(permissao.getDescricao() + " Atualizado");
    }

    private List<Permissao> buildPermissoes() {
        return Arrays.asList(permissao, Permissao.builder().nome("ROLE_USER").descricao("ROLE_USER").build());
    }
}