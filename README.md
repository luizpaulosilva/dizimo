Projeto deployado no Heroku - http://dizimo-api.herokuapp.com/dizimo/v1/swagger-ui.html

Para deploy é necessário comentar algumas informações, e alterar o application.yml conforme abaixo:
------------------------------------------------------------------------------------------------------------------
    server:
    #port: 9000
    servlet:
        context-path: /dizimo/v1

    spring:
        jpa:
        hibernate.ddl-auto: update
        show-sql: true
        properties:
            hibernate:
            dialect: org.hibernate.dialect.PostgreSQLDialect
            #dialect:  org.hibernate.dialect.MySQL5Dialect
            show_sql: true
            use_sql_comments: true
            format_sql: true
            type: trace

        datasource:
        driver-class-name: org.postgresql.Driver
        #driver-class-name:  com.mysql.cj.jdbc.Driver
        url: ${JDBC_DATASOURCE_URL}
        #url:  jdbc:mysql://localhost:3306/dizimo?useTimezone=true&serverTimezone=UTC
        #username: root
        #password: root
------------------------------------------------------------------------------------------------------------------